package com.feiyiproxy.pocketwifi.netty.vpntcp;

import android.annotation.SuppressLint;
import android.os.SystemClock;
import android.util.Log;
import com.feiyiproxy.pocketwifi.netty.NettyConstant;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * Netty客户端
 * Created by lmh on 11/18/2017.
 */
public class VpnTcpClient {

    public static final String TAG= "proxy_service_VpnTcp";

    private static VpnTcpClient nettyClient = new VpnTcpClient();

    private EventLoopGroup group;

    private VpnTcpListener listener;

    private Channel channel;

    private boolean isConnect = false;

    private int reconnectNum = Integer.MAX_VALUE;

    private long reconnectIntervalTime = 5 * 1000;

    public static VpnTcpClient getInstance() {
        return nettyClient;
    }

    public synchronized VpnTcpClient connect(String ip, int port) {
        String ipStr =ip;
        int portInt = port;
        if (!isConnect) {
            try {
                group = new NioEventLoopGroup();
                Bootstrap bootstrap = new Bootstrap()
                        .group(group)
                        .channel(NioSocketChannel.class)//TCP
                        .option(ChannelOption.SO_KEEPALIVE, true) //TCP_NODELAY
                        .handler(new VpnTcpClientInitializer(listener));

                ChannelFuture future = bootstrap.connect(ip,port).addListener(new ChannelFutureListener() {
                    @Override
                    public void operationComplete(ChannelFuture channelFuture){
                        if (channelFuture.isSuccess()) {
                            isConnect = true;
                            channel = channelFuture.channel();
                            Log.e(TAG , "VpnTcp Socket连接成功");
                        } else {
                            isConnect = false;
                            Log.e(TAG , "VpnTcp Socket连接失败");
                        }
                    }
                }).sync();
                //future.channel().closeFuture().sync();
            } catch (Exception e) {
                Log.e(TAG, ""+e.getMessage());
                listener.onVpnTcpStatusConnectChanged(NettyConstant.VpnTcp_STATUS_CONNECT_ERROR);
                reconnect(ipStr,portInt);
            }
        }
        return this;
    }

    public void disconnect() {
        group.shutdownGracefully();
    }

    @SuppressLint("LongLogTag")
    public void reconnect(String ip, int port) {
            SystemClock.sleep(reconnectIntervalTime);
            Log.e(TAG, "VpnTcp 重新连接");
            connect(ip ,port);
//        if (reconnectNum > 0 && !isConnect) {
//            reconnectNum--;
//            SystemClock.sleep(reconnectIntervalTime);
//            Log.e(TAG, "VpnTcp 重新连接");
//            disconnect();
//            connect(ip ,port);
//        } else {
//            disconnect();
//        }
    }

    /**
     * 发送数据给服务端
     * @param data
     * @param listener
     * @return
     */
    public boolean sendMsgToServer(Object data, ChannelFutureListener listener) {
        boolean flag = channel != null && isConnect;
        if (flag) {
            channel.writeAndFlush(data).addListener(listener);
        }
        return flag;
    }

    public void setReconnectNum(int reconnectNum) {
        this.reconnectNum = reconnectNum;
    }

    public void setReconnectIntervalTime(long reconnectIntervalTime) {
        this.reconnectIntervalTime = reconnectIntervalTime;
    }

    public boolean getConnectStatus() {
        return isConnect;
    }

    public void setConnectStatus(boolean status) {
        this.isConnect = status;
    }

    public void setListener(VpnTcpListener listener) {
        this.listener = listener;
    }
}
