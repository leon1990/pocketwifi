package com.feiyiproxy.pocketwifi.netty.vpnudp;

import java.util.concurrent.TimeUnit;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.CharsetUtil;

/**
 * Created by lmh on 11/18/2017.
 */
public class VpnUdpClientInitializer extends ChannelInitializer<NioDatagramChannel> {

    private VpnUdpListener listener;
    //private int WRITE_WAIT_SECONDS = 10;
    //private int READ_WAIT_SECONDS = 13;

    public VpnUdpClientInitializer(VpnUdpListener listener) {
        this.listener = listener;
    }

    @Override
    protected void initChannel(NioDatagramChannel ch) {

        ChannelPipeline pipeline = ch.pipeline();
        //pipeline.addLast("decoder", new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE,0,4,0,4));
        //pipeline.addLast("encoder", new LengthFieldPrepender(4, false));
        //pipeline.addLast(new LineBasedFrameDecoder(1024));
        //pipeline.addLast(new StringDecoder());
        pipeline.addLast(new LineBasedFrameDecoder(1024));
        pipeline.addLast(new StringDecoder());
        pipeline.addLast("decoder", new StringDecoder(CharsetUtil.UTF_8));
        pipeline.addLast("encoder", new StringEncoder(CharsetUtil.UTF_8));

        //SslContext sslCtx = SslContextBuilder.forClient().trustManager(InsecureTrustManagerFactory.INSTANCE).build();
        //pipeline.addLast(sslCtx.newHandler(ch.alloc()));    // 开启SSL
        pipeline.addLast(new LoggingHandler(LogLevel.INFO));// 开启日志，可以设置日志等级
        pipeline.addLast(new IdleStateHandler(5, 5, 10,TimeUnit.SECONDS));
        pipeline.addLast(new VpnUdpClientHandler(listener));

    }
}
