package com.feiyiproxy.pocketwifi.netty.vpntcp;

import android.util.Log;

import com.feiyiproxy.pocketwifi.netty.NettyConstant;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;

/**
 * Created by lmh on 11/18/2017.
 */
public class VpnTcpClientHandler extends SimpleChannelInboundHandler<Object>{

    public static final String TAG= "proxy_service_VpnTcp";


    private VpnTcpListener listener;

    public VpnTcpClientHandler(VpnTcpListener listener) {
        this.listener = listener;
    }

    /**
     * 连接成功
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        VpnTcpClient.getInstance().setConnectStatus(true);
        listener.onVpnTcpStatusConnectChanged(NettyConstant.VpnTcp_STATUS_CONNECT_SUCCESS);
    }

    /**
     * 和服务器断开了
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelInactive(ChannelHandlerContext ctx)  {
        VpnTcpClient.getInstance().setConnectStatus(false);
        listener.onVpnTcpStatusConnectChanged(NettyConstant.VpnTcp_STATUS_CONNECT_CLOSED);
        Log.e(TAG, "VpnTcp 和服务器断开了");
    }

    /**
     * 客户端收到消息
     */
    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Object str){
        listener.onVpnTcpResponse(str);
    }

	@Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {

		if (evt instanceof IdleStateEvent) {
			IdleStateEvent event = (IdleStateEvent) evt;
			if (event.state() == IdleState.READER_IDLE){//读超时
                ctx.close();
			}else if (event.state() == IdleState.WRITER_IDLE){//写超时
                listener.onVpnTcpHeartbeat();
			}else if(event.state() == IdleState.ALL_IDLE){
            }
		}else {
            super.userEventTriggered(ctx, evt);
        }

	}
}
