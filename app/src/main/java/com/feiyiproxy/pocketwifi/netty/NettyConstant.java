package com.feiyiproxy.pocketwifi.netty;

/**
 * Created by lmh on 11/18/2017.
 */

public class NettyConstant {

    public final static int VpnTcp_STATUS_CONNECT_SUCCESS = 200;
    public final static int VpnTcp_STATUS_CONNECT_CLOSED = 404;
    public final static int VpnTcp_STATUS_CONNECT_ERROR = 404;


    public final static int ProxyTcp_STATUS_CONNECT_SUCCESS = 200;
    public final static int ProxyTcp_STATUS_CONNECT_CLOSED = 404;
    public final static int ProxyTcp_STATUS_CONNECT_ERROR = 404;

    public final static int VpnUdp_STATUS_CONNECT_SUCCESS = 200;
    public final static int VpnUdp_STATUS_CONNECT_CLOSED = 404;
    public final static int VpnUdp_STATUS_CONNECT_ERROR = 404;


}
