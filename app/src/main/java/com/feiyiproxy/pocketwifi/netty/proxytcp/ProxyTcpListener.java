package com.feiyiproxy.pocketwifi.netty.proxytcp;


/**
 * Created by lmh on 11/18/2017.
 */
public interface ProxyTcpListener {

    /**
     * 当接收到系统消息
     */
    void onProxyTcpResponse(Object trs);


    /**
     * 发送心跳
     */
    void onProxyTcpHeartbeat();


    /**
     * 当服务状态发生变化时触发
     */
    void onProxyTcpStatusConnectChanged(int statusCode);

}
