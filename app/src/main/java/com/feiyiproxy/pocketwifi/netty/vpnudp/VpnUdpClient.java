package com.feiyiproxy.pocketwifi.netty.vpnudp;

import android.annotation.SuppressLint;
import android.os.SystemClock;
import android.util.Log;

import com.feiyiproxy.pocketwifi.netty.NettyConstant;

import java.net.InetSocketAddress;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.DatagramPacket;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.util.CharsetUtil;

/**
 * Netty客户端
 * Created by lmh on 11/18/2017.
 */
public class VpnUdpClient {

    public static final String TAG= "proxy_service_VpnUdp";

    private static VpnUdpClient nettyClient = new VpnUdpClient();

    private EventLoopGroup group;

    private VpnUdpListener listener;

    private Channel channel;

    private boolean isConnect = false;

    private int reconnectNum = Integer.MAX_VALUE;

    private long reconnectIntervalTime = 10 * 1000;

    public static VpnUdpClient getInstance() {
        return nettyClient;
    }

    public synchronized VpnUdpClient connect(String ip, int port) {
        String ipStr =ip;
        int portInt = port;
        if (!isConnect) {
            try {
                group = new NioEventLoopGroup();
                Bootstrap bootstrap = new Bootstrap().group(group)
                        .option(ChannelOption.SO_BROADCAST, true)
                        .channel(NioDatagramChannel.class)//UDP
                        .handler(new VpnUdpClientInitializer(listener));

                Channel ch = bootstrap.bind(0).sync().channel();
                // 向网段类所有机器广播发UDP
                ch.writeAndFlush(new DatagramPacket(Unpooled.copiedBuffer("VpnUdp--！！!",CharsetUtil.UTF_8),
                                new InetSocketAddress(ip,port))).sync();
                ch.closeFuture().await();
//                if(!ch.closeFuture().await(15000)){
//                    Log.e(TAG , "VpnUdp-----查询超时!");
//                }
//                bootstrap.connect(ip,port).addListener(new ChannelFutureListener() {
//                    @Override
//                    public void operationComplete(ChannelFuture channelFuture) {
//                        if (channelFuture.isSuccess()) {
//                            isConnect = true;
//                            channel = channelFuture.channel();
//                            Log.e(TAG , "VpnUdp Socket连接成功");
//                        } else {
//                            isConnect = false;
//                            Log.e(TAG , "VpnUdp Socket连接失败");
//                        }
//                    }
//                }).sync();

            } catch (Exception e) {
                Log.e(TAG, ""+e.getMessage());
                listener.onVpnUdpStatusConnectChanged(NettyConstant.VpnUdp_STATUS_CONNECT_ERROR);
                reconnect(ipStr,portInt);
            } finally {
                group.shutdownGracefully();
            }
        }
        return this;
    }

    public void disconnect() {
        group.shutdownGracefully();
    }

    @SuppressLint("LongLogTag")
    public void reconnect(String ip, int port) {
        if (reconnectNum > 0 && !isConnect) {
            reconnectNum--;
            SystemClock.sleep(reconnectIntervalTime);
            Log.e(TAG, "VpnUdp--------重新连接");
            disconnect();
            connect(ip ,port);
        } else {
            disconnect();
        }
    }

    /**
     * 发送数据给服务端
     * @param data
     * @param listener
     * @return
     */
    public boolean sendMsgToServer(Object data, ChannelFutureListener listener) {
        boolean flag = channel != null && isConnect;
        if (flag) {
            channel.writeAndFlush(data).addListener(listener);
        }
        return flag;
    }

    public void setReconnectNum(int reconnectNum) {
        this.reconnectNum = reconnectNum;
    }

    public void setReconnectIntervalTime(long reconnectIntervalTime) {
        this.reconnectIntervalTime = reconnectIntervalTime;
    }

    public boolean getConnectStatus() {
        return isConnect;
    }

    public void setConnectStatus(boolean status) {
        this.isConnect = status;
    }

    public void setListener(VpnUdpListener listener) {
        this.listener = listener;
    }
}
