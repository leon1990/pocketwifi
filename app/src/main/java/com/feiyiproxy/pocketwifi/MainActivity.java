package com.feiyiproxy.pocketwifi;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;

import com.feiyiproxy.pocketwifi.service.AssistService;
import com.feiyiproxy.pocketwifi.service.letPlatformService;

import java.util.Arrays;

import static java.lang.System.arraycopy;



public class MainActivity extends Activity {

    public static final String TAG= "MainActivity-log";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        Log.i(TAG, "onCreate" +
                "Thread ID = " + Thread.currentThread().getId() +
                "准备开启服务"
        );
        Intent intent = new Intent(MainActivity.this, letPlatformService.class);
        this.startService(intent);
//        Intent intentAssist = new Intent(MainActivity.this, AssistService.class);
//        this.startService(intentAssist);
//        Log.i(TAG, "getDeviceInfo" +getDeviceInfo());
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "onResume");
        super.onResume();
        finish();
    }


    @Override
    protected void onPause() {
        Log.i(TAG, "onPause");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.i(TAG, "onStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 获取设备信息
     */
    private String getDeviceInfo() {
        StringBuffer sb = new StringBuffer();
        sb.append("主板： " + Build.BOARD);
        sb.append("\n系统启动程序版本号： " + Build.BOOTLOADER);
        sb.append("\n系统定制商： " + Build.BRAND);
        sb.append("\ncpu指令集： " + Build.CPU_ABI);
        sb.append("\ncpu指令集2: " + Build.CPU_ABI2);
        sb.append("\n设置参数： " + Build.DEVICE);
        sb.append("\n显示屏参数：" + Build.DISPLAY);
        sb.append("\n无线电固件版本：" + Build.getRadioVersion());
        sb.append("\n硬件识别码： " + Build.FINGERPRINT);
        sb.append("\n硬件名称： " + Build.HARDWARE);
        sb.append("\nHOST: " + Build.HOST);
        sb.append("\nBuild.ID: " + Build.ID);
        sb.append("\n硬件制造商： " + Build.MANUFACTURER);
        sb.append("\n版本： " + Build.MODEL);
        sb.append("\n硬件序列号： " + Build.SERIAL);
        sb.append("\n手机制造商： " + Build.PRODUCT);
        sb.append("\n 描述Build的标签： " + Build.TAGS);
        sb.append("\nTIME: " + Build.TIME);
        sb.append("\nbuilder类型: " + Build.TYPE);
        sb.append("\nUSER: " + Build.USER);
        return sb.toString();
    }

}
