package com.feiyiproxy.pocketwifi.bean;

public class Vpnjson {

    public Vpn vpn;

    public static class Vpn{
        public boolean enable;
        public long conntime;
        public long timestamp;
        public int tcpport;
        public int udpport;
        public String ip;
        public int alive;
    }

    public Proxy proxy;

    public static class Proxy{
        public boolean enable;
        public long conntime;
        public long timestamp;
        public String host;
        public String port;
        public int alive;
    }

}
