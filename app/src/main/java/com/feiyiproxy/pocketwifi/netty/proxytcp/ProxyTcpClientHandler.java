package com.feiyiproxy.pocketwifi.netty.proxytcp;

import android.util.Log;

import com.feiyiproxy.pocketwifi.netty.NettyConstant;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;

/**
 * Created by lmh on 11/18/2017.
 */
public class ProxyTcpClientHandler extends SimpleChannelInboundHandler<Object> {


    public static final String TAG= "proxy_service_ProxyTcp";

    private ProxyTcpListener listener;

    public ProxyTcpClientHandler(ProxyTcpListener listener) {
        this.listener = listener;
    }

    /**
     * 连接成功
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx){
        ProxyTcpClient.getInstance().setConnectStatus(true);
        listener.onProxyTcpStatusConnectChanged(NettyConstant.ProxyTcp_STATUS_CONNECT_SUCCESS);
    }

    /**
     * 和服务器断开了
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelInactive(ChannelHandlerContext ctx){
        ProxyTcpClient.getInstance().setConnectStatus(false);
        listener.onProxyTcpStatusConnectChanged(NettyConstant.ProxyTcp_STATUS_CONNECT_CLOSED);
        Log.e(TAG, "ProxyTcp 和服务器断开了");
    }

    /**
     * 客户端收到消息
     */
    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Object str){
        listener.onProxyTcpResponse(str);
    }

	@Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {

		if (evt instanceof IdleStateEvent) {
			IdleStateEvent event = (IdleStateEvent) evt;
			if (event.state() == IdleState.READER_IDLE){
				ctx.close();
			}else if (event.state() == IdleState.WRITER_IDLE){
                listener.onProxyTcpHeartbeat();
			}else if (event.state() == IdleState.ALL_IDLE){//都超时

            }
		}else {
            super.userEventTriggered(ctx, evt);
        }
	}
}
