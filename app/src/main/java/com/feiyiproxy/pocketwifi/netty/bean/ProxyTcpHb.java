package com.feiyiproxy.pocketwifi.netty.bean;

import static java.lang.System.arraycopy;

public class ProxyTcpHb {

    public static byte[] hbStruct() {
//       uint8_t    1 byte
//       uint16_t   2 byte
//       uint32_t   4 byte
//       uint64_t   8 byte
        byte[] hbStruct = new byte[55];

        //45 byte
        byte[] hdrStruct = new byte[18];
        //magic 4 byte
        hdrStruct[0] = (byte) ((SocketConstant.IP_PROXY_MAGIC_NUMBER >> 24) & 0xFF);
        hdrStruct[1] = (byte) ((SocketConstant.IP_PROXY_MAGIC_NUMBER >> 16) & 0xFF);
        hdrStruct[2] = (byte) ((SocketConstant.IP_PROXY_MAGIC_NUMBER >> 8) & 0xFF);
        hdrStruct[3] = (byte) (SocketConstant.IP_PROXY_MAGIC_NUMBER & 0xFF);
        //type 2 byte
        hdrStruct[4] = 0;
        hdrStruct[5] = SocketConstant.IP_PROXY_CMD_KEEPALIVE;
        //hdr_len 2 byte
        hdrStruct[6] = 0;
        hdrStruct[7] = 17;//len
        //seq_id 4 byte
        hdrStruct[8] = 0;
        hdrStruct[9] = 0;
        hdrStruct[10] = 0;
        hdrStruct[11] = 0;
        //debug_id 4 byte
        hdrStruct[12] = 0;
        hdrStruct[13] = 0;
        hdrStruct[14] = 0;
        hdrStruct[15] = 0;
        //total_len 2 byte
        hdrStruct[16] = 0;
        hdrStruct[17] = 53;

        //45 byte
        byte[] nodeStruct = new byte[37];
        //device_name 1 byte
        nodeStruct[0] = SocketConstant.VERSION_NUMBER;
        //version 2 byte
        nodeStruct[1] = 0;
        nodeStruct[2] = 0;
        //devid 8 byte
        nodeStruct[3] = 0;
        nodeStruct[4] = 0;
        nodeStruct[5] = 0;
        nodeStruct[6] = 0;
        nodeStruct[7] = 0;
        nodeStruct[8] = 0;
        nodeStruct[9] = 0;
        nodeStruct[10] = 0;
        //cpu_percentage 1byte
        nodeStruct[11] = SocketConstant.CPU_PERCENT;
        //mem_total 4byte
        nodeStruct[12] = (byte) ((SocketConstant.MEM_TOTAL >> 24) & 0xFF);
        nodeStruct[13] = (byte) ((SocketConstant.MEM_TOTAL >> 16) & 0xFF);
        nodeStruct[14] = (byte) ((SocketConstant.MEM_TOTAL >> 8) & 0xFF);
        nodeStruct[15] = (byte) (SocketConstant.MEM_TOTAL & 0xFF);
        //mem_used 4 byte
        nodeStruct[16] = (byte) ((SocketConstant.MEM_USED >> 24) & 0xFF);
        nodeStruct[17] = (byte) ((SocketConstant.MEM_USED >> 16) & 0xFF);
        nodeStruct[18] = (byte) ((SocketConstant.MEM_USED >> 8) & 0xFF);
        nodeStruct[19] = (byte) (SocketConstant.MEM_USED & 0xFF);
        //broadband_in 8 byte
        nodeStruct[20] = (byte) ((SocketConstant.BROADBAND_IN >> 8 * 7) & 0xFF);
        nodeStruct[21] = (byte) ((SocketConstant.BROADBAND_IN >> 8 * 6) & 0xFF);
        nodeStruct[22] = (byte) ((SocketConstant.BROADBAND_IN >> 8 * 5) & 0xFF);
        nodeStruct[23] = (byte) ((SocketConstant.BROADBAND_IN >> 8 * 4) & 0xFF);
        nodeStruct[24] = (byte) ((SocketConstant.BROADBAND_IN >> 24) & 0xFF);
        nodeStruct[25] = (byte) ((SocketConstant.BROADBAND_IN >> 16) & 0xFF);
        nodeStruct[26] = (byte) ((SocketConstant.BROADBAND_IN >> 8) & 0xFF);
        nodeStruct[27] = (byte) (SocketConstant.BROADBAND_IN  & 0xFF);
        //broadband_out 8 byte
        nodeStruct[28] = (byte) ((SocketConstant.BROADBAND_OUT >> 8 * 7) & 0xFF);
        nodeStruct[29] = (byte) ((SocketConstant.BROADBAND_OUT >> 8 * 6) & 0xFF);
        nodeStruct[30] = (byte) ((SocketConstant.BROADBAND_OUT >> 8 * 5) & 0xFF);
        nodeStruct[31] = (byte) ((SocketConstant.BROADBAND_OUT >> 8 * 4) & 0xFF);
        nodeStruct[32] = (byte) ((SocketConstant.BROADBAND_OUT >> 24) & 0xFF);
        nodeStruct[33] = (byte) ((SocketConstant.BROADBAND_OUT >> 16) & 0xFF);
        nodeStruct[34] = (byte) ((SocketConstant.BROADBAND_OUT >> 8) & 0xFF);
        nodeStruct[35] = (byte) (SocketConstant.BROADBAND_OUT & 0xFF);
        //status 1 byte
        nodeStruct[36] = (byte) 600;

        //合并字节数组
        arraycopy(hdrStruct, 0, hbStruct, 0, hdrStruct.length);
        arraycopy(nodeStruct, 0, hbStruct, hdrStruct.length, nodeStruct.length);
//        arraycopy(nameArr, 0, hbStruct, hdrStruct.length+macArr.length, nameArr.length);
//        Log.i("msgStruct","msgStruct--->"+msgStruct.length);
//        Log.i("msgStruct","msgStruct--->"+Arrays.toString(msgStruct));
        return hbStruct;
    }
}
