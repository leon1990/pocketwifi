package com.feiyiproxy.pocketwifi.netty.vpntcp;


/**
 * Created by lmh on 11/18/2017.
 */
public interface VpnTcpListener {

    /**
     * 当接收到系统消息
     */
    void onVpnTcpResponse(Object trs);

    /**
     * 发送心跳
     */
    void onVpnTcpHeartbeat();

    /**
     * 当服务状态发生变化时触发
     */
    void onVpnTcpStatusConnectChanged(int statusCode);

}
