package com.feiyiproxy.pocketwifi.netty.bean;

public class SocketConstant {

    //VpnTcp
    public final static int LET_VPN_MAGIC = 0xfadb9812;
    public final static int LET_VPN_KEEPALIVE = 1;
    public final static int LET_VPN_AUTH = 2;
    public final static String NAME_STR = "qazwsxedcrfvbgt\0";


    //ProxyTcp
    public final static int IP_PROXY_MAGIC_NUMBER = 0x12348898;
    public final static int IP_PROXY_CMD_KEEPALIVE = 5;

    public final static int VERSION_NUMBER = 101;

    //short 2byte | int 4byte | long 8byte
    public final static int CPU_PERCENT = 8;
    public final static int MEM_TOTAL = 33554432;
    public final static int MEM_USED = 10000000;
    public final static long BROADBAND_IN = 100000;
    public final static long BROADBAND_OUT = 100000;

    // VpnUdp

}
