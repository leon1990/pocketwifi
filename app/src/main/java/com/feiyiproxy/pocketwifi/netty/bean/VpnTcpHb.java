package com.feiyiproxy.pocketwifi.netty.bean;

import android.util.Log;

import java.util.Arrays;

public class VpnTcpHb {

    public static byte[] hbStruct() {

        byte[] hbStruct = new byte[21];//21 byte
        //magic 4 byte
        hbStruct[0] = (byte) ((SocketConstant.LET_VPN_MAGIC >> 24) & 0xFF);
        hbStruct[1] = (byte) ((SocketConstant.LET_VPN_MAGIC >> 16) & 0xFF);
        hbStruct[2] = (byte) ((SocketConstant.LET_VPN_MAGIC >> 8) & 0xFF);
        hbStruct[3] = (byte) (SocketConstant.LET_VPN_MAGIC & 0xFF);
        //ip 4byte
        hbStruct[4] = 0;
        hbStruct[5] = 0;
        hbStruct[6] = 0;
        hbStruct[7] = 0;
        //port 2byte
        hbStruct[8] = 0;
        hbStruct[9] = 0;
        //ip 4byte
        hbStruct[10] = 0;
        hbStruct[11] = 0;
        hbStruct[12] = 0;
        hbStruct[13] = 0;
        //port 2byte
        hbStruct[14] = 0;
        hbStruct[15] = 0;
        //protocol 1byte
        hbStruct[16] = 0;
        //type 2byte
        hbStruct[17] = 0;
        hbStruct[18] = SocketConstant.LET_VPN_KEEPALIVE;
        //len 2byte
        hbStruct[19] = 0;
        hbStruct[20] = 21;

        Log.i("msgStruct","hbStruct--->"+hbStruct.length);
        Log.i("msgStruct","hbStruct--->"+Arrays.toString(hbStruct));
        return hbStruct;
    }
}
