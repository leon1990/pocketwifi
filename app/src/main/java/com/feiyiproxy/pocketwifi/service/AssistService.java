package com.feiyiproxy.pocketwifi.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class AssistService extends Service {

    public static final String TAG= "AssistService";
    private Context mContext;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.ACTION_TIME_TICK.equals(intent.getAction())){
                Toast.makeText(context,"TIME_TICK",Toast.LENGTH_SHORT).show();
            }
        }
    };


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mContext = this;
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_TIME_TICK);
        registerReceiver(broadcastReceiver, filter);
        Toast.makeText(mContext,TAG,Toast.LENGTH_SHORT).show();
        return START_STICKY;
//        return super.onStartCommand(intent, START_STICKY, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy - Thread ID = " + Thread.currentThread().getId());
        Intent intent = new Intent(this, AssistService.class);
        startService(intent);
    }

}
