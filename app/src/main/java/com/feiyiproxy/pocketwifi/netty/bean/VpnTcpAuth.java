package com.feiyiproxy.pocketwifi.netty.bean;

import android.util.Log;

import com.feiyiproxy.pocketwifi.util.OS;

import java.util.Arrays;

import static java.lang.System.arraycopy;

public class VpnTcpAuth {

    public static byte[] AuthStruct() {

        //45 byte
        byte[] AuthStruct = new byte[45];
        //8 byte
        byte[] macArr = OS.macByte();//mac地址：16进制 6个字节
        //16 byte
        byte[] nameArr= SocketConstant.NAME_STR.getBytes();
        //21 byte
        byte[] hbStruct = new byte[21];
        //magic 4 byte
        hbStruct[0] = (byte) ((SocketConstant.LET_VPN_MAGIC >> 24) & 0xFF);
        hbStruct[1] = (byte) ((SocketConstant.LET_VPN_MAGIC >> 16) & 0xFF);
        hbStruct[2] = (byte) ((SocketConstant.LET_VPN_MAGIC >> 8) & 0xFF);
        hbStruct[3] = (byte) (SocketConstant.LET_VPN_MAGIC & 0xFF);
        //ip 4byte
        hbStruct[4] = 0;
        hbStruct[5] = 0;
        hbStruct[6] = 0;
        hbStruct[7] = 0;
        //port 2byte
        hbStruct[8] = 0;
        hbStruct[9] = 0;
        //ip 4byte
        hbStruct[10] = 0;
        hbStruct[11] = 0;
        hbStruct[12] = 0;
        hbStruct[13] = 0;
        //port 2byte
        hbStruct[14] = 0;
        hbStruct[15] = 0;
        //protocol 1byte
        hbStruct[16] = 0;
        //type 2byte
        hbStruct[17] = 0;
        hbStruct[18] = SocketConstant.LET_VPN_AUTH;
        //len 2byte
        hbStruct[19] = 0;
        hbStruct[20] = 45;

        //合并字节数组
        arraycopy(hbStruct, 0, AuthStruct, 0, hbStruct.length);
        arraycopy(macArr, 0, AuthStruct, hbStruct.length, macArr.length);
        arraycopy(nameArr, 0, AuthStruct, hbStruct.length+macArr.length, nameArr.length);

        Log.i("msgStruct","msgStruct--->"+AuthStruct.length);
        Log.i("msgStruct","msgStruct--->"+Arrays.toString(AuthStruct));
        return AuthStruct;
    }

}
