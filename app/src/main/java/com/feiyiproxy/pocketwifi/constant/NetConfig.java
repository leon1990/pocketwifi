package com.feiyiproxy.pocketwifi.constant;

import android.os.Build;

import com.feiyiproxy.pocketwifi.util.OS;

import java.net.NetworkInterface;
import java.util.Collections;
import java.util.Enumeration;

public class NetConfig {

    public static String getParm() {

        String md5Str = "8df645543e953b0931d808c36be4bd0b";
        String macStr = OS.getLocalMac();
        String interfacesStr = "br0-eth0";
        String chipStr = ""+Build.CPU_ABI;
        String machineStr = ""+Build.CPU_ABI;
        String osStr = "Android";
        String versionStr = Build.VERSION.RELEASE;
        String archStr =""+Build.CPU_ABI;
        String manuStr = "feiyi012";
        String json = "{\n" +
                "    \"self\":{\n" +
                "        \"md5\":\""+md5Str+"\"\n" +
                "    },\n" +
                "    \"programs\":[],\n" +
                "    \"system\":{\n" +
                "        \"mac\":\""+macStr+"\",\n" +
                "        \"interfaces\":\""+interfacesStr+"\",\n" +
                "        \"chip\":\""+chipStr+"\",\n" +
                "        \"machine\":\""+machineStr+"\",\n" +
                "        \"os\":\""+osStr+"\",\n" +
                "        \"version\":\""+versionStr+"\",\n" +
                "        \"arch\":\""+archStr+"\",\n" +
                "        \"manu\":\""+manuStr+"\"\n" +
                "    }\n" +
                "}";
        return json;
    }

    public static String getCoreParm() {
        String SerialNumber = Build.SERIAL;
        String devtype = "feiyi010";
        String deviceID = "52228567968";//设备硬件id
        long timeStamp = System.currentTimeMillis();
        boolean isVpn = isVpnCon();

        String vpnIP = "360000000";
        String vpnConntime = "60.191.57.80";
        String vpnTcpport = "55444";
        String vpnUdpport = "54333";

        String proxyConntime = "86400";
        String proxyHost = "master.feiyiproxy.com";
        String proxyPort = "87";

        String json = "{\n" +
                " \"devtype\": \""+devtype+"\",\n" +
                " \"devid\": "+deviceID+",\n" +
                " \"vpn\": {\n" +
                "       \"enable\": "+isVpn+",\n" +
                "       \"timestamp\": "+timeStamp+",\n" +
                "       \"conn-time\": "+vpnIP+",\n" +
                "       \"ip\": \""+vpnConntime+"\",\n" +
                "       \"tcp-port\": "+vpnTcpport+",\n" +
                "       \"udp-port\": "+vpnUdpport+"\n" +
                " },\n" +
                " \"proxy\": {\n" +
                "       \"enable\": "+isVpn+",\n" +
                "       \"timestamp\":  "+timeStamp+",\n" +
                "       \"conn-time\": "+proxyConntime+",\n" +
                "       \"host\": \""+proxyHost+"\",\n" +
                "       \"port\": "+proxyPort+"\n" +
                " }\n" +
                "}";
        return json;
    }

    private static boolean isVpnCon() {
        try {
            Enumeration<NetworkInterface> niList = NetworkInterface.getNetworkInterfaces();
            if(niList != null) {
                for (NetworkInterface intf : Collections.list(niList)) {
                    if(!intf.isUp() || intf.getInterfaceAddresses().size() == 0) {
                        continue;
                    }
                    if ("tun0".equals(intf.getName()) || "ppp0".equals(intf.getName())){
                        return true;
                    }
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return false;
    }
}
