package com.feiyiproxy.pocketwifi.netty.vpnudp;



/**
 * Created by lmh on 11/18/2017.
 */
public interface VpnUdpListener {

    /**
     * 当接收到系统消息
     */
    void onVpnUdpResponse(Object trs);


    /**
     * 发送心跳
     */
    void onVpnUdpHeartbeat();


    /**
     * 当服务状态发生变化时触发
     */
    void onVpnUdpStatusConnectChanged(int statusCode);

}
