package com.feiyiproxy.pocketwifi.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import com.feiyiproxy.pocketwifi.bean.Fyconf;
import com.feiyiproxy.pocketwifi.bean.Md5Bean;
import com.feiyiproxy.pocketwifi.bean.Vpnjson;
import com.feiyiproxy.pocketwifi.constant.API;
import com.feiyiproxy.pocketwifi.constant.Constants;
import com.feiyiproxy.pocketwifi.constant.NetConfig;
import com.feiyiproxy.pocketwifi.netty.bean.ProxyTcpHb;
import com.feiyiproxy.pocketwifi.netty.bean.VpnTcpHb;
import com.feiyiproxy.pocketwifi.netty.bean.VpnTcpAuth;
import com.feiyiproxy.pocketwifi.netty.proxytcp.ProxyTcpClient;
import com.feiyiproxy.pocketwifi.netty.proxytcp.ProxyTcpListener;
import com.feiyiproxy.pocketwifi.netty.vpntcp.VpnTcpClient;
import com.feiyiproxy.pocketwifi.netty.NettyConstant;
import com.feiyiproxy.pocketwifi.netty.vpntcp.VpnTcpListener;
import com.feiyiproxy.pocketwifi.netty.vpnudp.VpnUdpClient;
import com.feiyiproxy.pocketwifi.netty.vpnudp.VpnUdpListener;
import com.feiyiproxy.pocketwifi.util.NetworkUtil;
import com.feiyiproxy.pocketwifi.util.ShellUtil;
import com.feiyiproxy.pocketwifi.util.WeakHandler;
import com.google.gson.Gson;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;

/**
 * IP代理程序（后台运行）
 */
public class letPlatformService extends Service implements VpnTcpListener,ProxyTcpListener,VpnUdpListener {

    public static final String TAG= "proxy_service";

    private Context mContext;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    
    private Md5Bean md5Bean;
    private String filePath = "";
    private String fileBinPath = "";

    private int vpn_udpport;
    private int vpn_tcpport;
    private String vpn_ip ;

    private int proxy_tcpport;
    private String proxy_ip;

    private static ScheduledExecutorService mScheduledExecutor;
    private static ScheduledExecutorService vpnTcpExecutor;
    private static ScheduledExecutorService vpnUdpExecutor;
    private static ScheduledExecutorService proxyTcpExecutor;

    //VpnTcp  ProxyTcp VpnUdp
    private static byte[] vpnTcpAuth;
    private static byte[] vpnTcpHb;
    private static byte[] proxyTcpHb;

    static{
        mScheduledExecutor = Executors.newScheduledThreadPool(6);
        vpnTcpExecutor = Executors.newSingleThreadScheduledExecutor();
        vpnUdpExecutor = Executors.newSingleThreadScheduledExecutor();
        proxyTcpExecutor = Executors.newSingleThreadScheduledExecutor();


        //VpnTcp  ProxyTcp VpnUdp
        vpnTcpAuth = VpnTcpAuth.AuthStruct();
        vpnTcpHb = VpnTcpHb.hbStruct();
        proxyTcpHb = ProxyTcpHb.hbStruct();
    }
  
    
    //OS  Handler
    private Handler mHandler = new ProxyHandler(this);
    private class ProxyHandler extends WeakHandler<letPlatformService> {
        ProxyHandler(letPlatformService owner) {
            super(owner);
        }

        @Override
        public void handleMessage(Message msg) {
            final letPlatformService service = getOwner();
            if (service == null) return;
            switch (msg.what) {
                case 1:
                    //01:VPN Socket TCP长连接
                    connectVpnTcpSocket();
                    break;
                case 2:
                   //02:VPN Socket UDP长连接
                    connectProxyTcpSocket();
                    break;
                case 3:
                    //03:proxy Socket TCP长连接
                    connectVpnUdpSocket();
                    break;
            }
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        Log.i(TAG, "onCreate" );
    }

    @SuppressLint("WrongConstant")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStartCommand" );
        String getPath = getExternalCacheDir().getPath();//该目录pm install 安装成功
        //String getPath = this.getCacheDir().getPath();//该目录pm install 安装失败
        filePath = getPath;
        Log.i(TAG, "getPath= " + getPath);
        sp = mContext.getSharedPreferences("shareData", Context.MODE_PRIVATE);
        editor = sp.edit();

        //定时任务 + 线程池
        mScheduledExecutor.scheduleAtFixedRate(mTimerRunnable,100,
                Constants.MAIN_TIME,TimeUnit.MILLISECONDS);

        //VpnTcp  ProxyTcp  VpnUdp
        VpnTcpClient.getInstance().setListener(this);
        ProxyTcpClient.getInstance().setListener(this);
        VpnUdpClient.getInstance().setListener(this);
        
        return START_STICKY;
       // return super.onStartCommand(intent, START_STICKY, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "onBind Thread ID = " + Thread.currentThread().getId());
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy - Thread ID = " + Thread.currentThread().getId());
        //VpnTcp--------
        VpnTcpClient.getInstance().setReconnectNum(0);
        VpnTcpClient.getInstance().disconnect();
        //ProxyTcp------
        ProxyTcpClient.getInstance().setReconnectNum(0);
        ProxyTcpClient.getInstance().disconnect();
        //VpnUdp--------
        ProxyTcpClient.getInstance().setReconnectNum(0);
        ProxyTcpClient.getInstance().disconnect();
        //---------------
        if (mScheduledExecutor != null) {
            mScheduledExecutor.shutdown();
            mScheduledExecutor = null;
        }
        if (vpnUdpExecutor != null) {
            vpnUdpExecutor.shutdown();
            vpnUdpExecutor = null;
        }
        if (vpnTcpExecutor != null) {
            vpnTcpExecutor.shutdown();
            vpnTcpExecutor = null;
        }
        if (proxyTcpExecutor != null) {
            proxyTcpExecutor.shutdown();
            proxyTcpExecutor = null;
        }
        Intent intent = new Intent(this, letPlatformService.class);
        startService(intent);
    }

    //TODO VpnTcp ***********************************************

    //0:连接Socket
    private void connectVpnTcpSocket() {
        boolean connectStatus = VpnTcpClient.getInstance().getConnectStatus();
        if (!connectStatus) {
            Log.e(TAG , "connectStatus = "+ connectStatus);



            vpnTcpExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    VpnTcpClient.getInstance().connect(vpn_ip,vpn_tcpport);//连接服务器
                }
            });
        }else {
            Log.e(TAG , "connectStatus "+connectStatus);
        }
    }

    //1:连接状态监听
    @Override
    public void onVpnTcpStatusConnectChanged(int statusCode){ //Socket连接成功

        if (statusCode == NettyConstant.VpnTcp_STATUS_CONNECT_SUCCESS) {//发送认证数据
            Log.i(TAG,"VpnTcp connect statusCode "+statusCode);


            ByteBuf vpnTcpMsgBuf = Unpooled.buffer(vpnTcpAuth.length);
            vpnTcpMsgBuf.writeBytes(vpnTcpAuth);
            Log.i(TAG,"vpnTcpMsg--->"+vpnTcpAuth.length);
            Log.i(TAG,"vpnTcpMsg--->"+Arrays.toString(vpnTcpAuth));
            VpnTcpClient.getInstance().sendMsgToServer(vpnTcpMsgBuf, new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) {
                    if (future.isSuccess()) {
                        Log.d(TAG, "databody Write auth successful");
                    } else {
                        Log.d(TAG, "databody Write auth error");
                    }
                }
            });
        } else {
            VpnTcpClient.getInstance().reconnect(vpn_ip,vpn_tcpport);
            Log.d(TAG, "VpnTcp connect status:" + statusCode);
        }
    }

    //2:发送心跳
    @Override
    public void onVpnTcpHeartbeat( ) {
        ByteBuf vpnTcpHbBuf = Unpooled.buffer(vpnTcpHb.length);
        vpnTcpHbBuf.writeBytes(vpnTcpHb);
        Log.i(TAG,"vpnTcpHb--->"+vpnTcpHb.length);
        Log.i(TAG,"vpnTcpHb--->"+Arrays.toString(vpnTcpHb));
        VpnTcpClient.getInstance().sendMsgToServer(vpnTcpHbBuf, new ChannelFutureListener() {    //3
            @Override
            public void operationComplete(ChannelFuture future) {
                if (future.isSuccess()) {
                    Log.d(TAG, "Write heartbeat successful");
                } else {
                    Log.e(TAG, "Write heartbeat error");
                }
            }
        });
    }

    //3:接收数据
    @Override
    public void onVpnTcpResponse(Object msg) {
        ByteArrayOutputStream bos;
        ObjectOutputStream oos;
        try {
            bos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(bos);
            oos.writeObject(msg);
            oos.flush();
            byte[] responseByte = bos.toByteArray ();
            Log.i(TAG,"onVpnTcpResponse--->"+responseByte.length);
            Log.i(TAG,"onVpnTcpResponse--->"+Arrays.toString(responseByte));
            ByteBuf responseBuf = Unpooled.buffer(responseByte.length);
            responseBuf.writeBytes(responseByte);
            //Log.i(TAG,"onVpnTcpResponse--->"+Arrays.toString(responseBuf.array()));
            oos.close();
            bos.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
//        ByteBuf result = (ByteBuf) trs;
//        byte[] bytes = new byte[result.readableBytes()];
//        result.readBytes(bytes);
//        String body = new String(bytes,CharsetUtil.UTF_8);//GBK
    }

    //TODO ProxyTcp ***********************************************


    //0:连接socket
    private void connectProxyTcpSocket() {
        boolean connectStatus = ProxyTcpClient.getInstance().getConnectStatus();
        if (!connectStatus) {
            Log.e(TAG , "ProxyTcp 连接成功= "+ connectStatus);
            Log.e(TAG , "ProxyTcp  proxy_ip = "+ proxy_ip+"  proxy_tcpport = "+ proxy_tcpport);
            proxyTcpExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    ProxyTcpClient.getInstance().connect(proxy_ip,proxy_tcpport);
                }
            });
        }else {
            Log.e(TAG , "ProxyTcp 连接失败="+connectStatus);
        }
    }

    //1：连接状态监听
    @Override
    public void onProxyTcpStatusConnectChanged(int statusCode) {
        if (statusCode == NettyConstant.ProxyTcp_STATUS_CONNECT_SUCCESS) {
            Log.d(TAG, "ProxyTcp CONNECT_SUCCESS:" + statusCode);
//            //发送数据
//            ByteBuf proxyTcpHbBuf = Unpooled.buffer(proxyTcpHb.length);
//            proxyTcpHbBuf.writeBytes(proxyTcpHb);
//            Log.i(TAG,"ProxyTcp--->"+proxyTcpHb.length);
//            Log.e(TAG,"ProxyTcp--->"+Arrays.toString(proxyTcpHb));
//            ProxyTcpClient.getInstance().sendMsgToServer(proxyTcpHbBuf, new ChannelFutureListener() {
//                @Override
//                public void operationComplete(ChannelFuture future) {
//                    if (future.isSuccess()) {
//                        Log.d(TAG, "ProxyTcp Write data successful");
//                    } else {
//                        Log.d(TAG, "ProxyTcp Write data error");
//                    }
//                }
//            });

        } else {
            Log.d(TAG, "ProxyTcp CONNECT_ERROR:" + statusCode);
            VpnTcpClient.getInstance().reconnect(vpn_ip,vpn_tcpport);
        }
    }
    //2：发送心跳
    @Override
    public void onProxyTcpHeartbeat() {
        ByteBuf proxyTcpHbBuf = Unpooled.buffer(proxyTcpHb.length);
        proxyTcpHbBuf.writeBytes(proxyTcpHb);
        Log.i(TAG,"ProxyTcp--->"+proxyTcpHb.length);
        Log.e(TAG,"ProxyTcp--->"+Arrays.toString(proxyTcpHb));
        ProxyTcpClient.getInstance().sendMsgToServer(proxyTcpHbBuf, new ChannelFutureListener() {    //3
            @Override
            public void operationComplete(ChannelFuture future) {
                if (future.isSuccess()) {                //4
                    Log.d(TAG, "ProxyTcp Write Heartbeat successful ----------");
                } else {
                    Log.d(TAG, "ProxyTcp Write Heartbeat error ----------");
                }
            }
        });
    }
    //3：接收数据
    @Override
    public void onProxyTcpResponse(Object msg) {

        ByteArrayOutputStream bos;
        ObjectOutputStream oos;
        try {
            bos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(bos);
            oos.writeObject(msg);
            oos.flush();
            byte[] responseByte = bos.toByteArray ();
            Log.i(TAG,"ProxyTcp Response--->"+responseByte.length);
            Log.i(TAG,"ProxyTcp Response--->"+Arrays.toString(responseByte));
            ByteBuf responseBuf = Unpooled.buffer(responseByte.length);
            responseBuf.writeBytes(responseByte);
            //.i(TAG,"ProxyTcp Response--->"+Arrays.toString(responseBuf.array()));
            oos.close();
            bos.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

//        ByteBuf result = (ByteBuf) trs;
//        byte[] bytes = new byte[result.readableBytes()];
//        result.readBytes(bytes);
//        String resultStr = new String(bytes);
    }

    //TODO VpnUdp ***********************************************

    //接收数据
    @Override
    public void onVpnUdpResponse(Object trs) {

        ByteArrayOutputStream bos;
        ObjectOutputStream oos;
        try {
            bos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(bos);
            oos.writeObject(trs);
            oos.flush();
            byte[] responseByte = bos.toByteArray ();
            Log.i(TAG,"VpnUdp Response--->"+responseByte.length);
            Log.i(TAG,"VpnUdp Response--->"+Arrays.toString(responseByte));
            ByteBuf responseBuf = Unpooled.buffer(responseByte.length);
            responseBuf.writeBytes(responseByte);
            //.i(TAG,"ProxyTcp Response--->"+Arrays.toString(responseBuf.array()));
            oos.close();
            bos.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    //发送心跳数据
    @Override
    public void onVpnUdpHeartbeat() {

        DatagramPacket data = new DatagramPacket(proxyTcpHb, proxyTcpHb.length);
        ProxyTcpClient.getInstance().sendMsgToServer(data, new ChannelFutureListener() {    //3
            @Override
            public void operationComplete(ChannelFuture future) {
                if (future.isSuccess()) {                //4
                    Log.d(TAG, "VpnUdp Write Heartbeat successful ----------");
                } else {
                    Log.d(TAG, "VpnUdp Write Heartbeat error ----------");
                }
            }
        });
    }
    //发送认证数据
    @Override
    public void onVpnUdpStatusConnectChanged(int statusCode) {
        Log.d(TAG, "VpnUdp onVpnUdpStatusConnectChanged "+statusCode );
    }

    private void connectVpnUdpSocket() {
        boolean  connectStatus = VpnUdpClient.getInstance().getConnectStatus();
        if (!connectStatus) {
            Log.e(TAG , "VpnUdp  vpn_ip = "+vpn_ip+" vpn_udpport="+vpn_udpport);

            vpnUdpExecutor.execute(new Runnable() {
                @Override
                public void run() {

                    VpnUdpClient.getInstance().connect(vpn_ip,vpn_udpport);//连接服务器
                }
            });
        }else {
            Log.e(TAG , "VpnUdp = isConnect");
        }
    }

    //TODO  ************************************************

    private final Runnable mTimerRunnable = new Runnable() {
        @Override
        public void run() {
            try {
                String result = NetworkUtil.doPost(API.P_URL,NetConfig.getParm());
                Gson mgson = new Gson();
                md5Bean = mgson.fromJson(result, Md5Bean.class);

                //01 删除文件夹下的所有文件
                delAllFile(filePath);
                //02 下载文件
                File file = NetworkUtil.downloadFile(md5Bean.self.url, filePath);
                fileBinPath =file.getPath();
                String fileMd5 = getFileMD5(fileBinPath);
                Log.i(TAG, "md5Bean=" + md5Bean.self.url);
                Log.i(TAG, "md5Bean=" + md5Bean.self.md5);
                Log.i(TAG, "fileMd5= " + fileMd5);
                Log.i(TAG, "filePath= " + filePath);
                if (md5Bean.self.md5.equals(fileMd5)) {
                    //copyAssets(filePath + "/", "app-debug.apk");//test
                    Log.i(TAG, "Md5值相等-->解压.bin文件");
                    analyseFile(fileBinPath); //处理.bin文件
                } else {
                    Log.i(TAG, "Md5值不相等");
                }
            } catch (Exception e) {
                Log.i(TAG, ""+e.getMessage());
                //e.printStackTrace();
            }
        }
    };

    private String getJson(String fileName) {
        File file = new File(fileName);
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        StringBuilder stringBuilder = new StringBuilder();
        try {
            InputStream is =new FileInputStream(file);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i("analyseFile", "" + stringBuilder.toString());
        return stringBuilder.toString();
    }

    //分解下载的bin文件
    private void analyseFile(String path){

        File file = new File(path);
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte fileByte[] = new byte[120000];
        FileInputStream finFile = null;
        FileOutputStream foutFileFir = null;
        FileOutputStream foutFileSec = null;
        FileOutputStream foutFileThi = null;
        try {
            finFile = new FileInputStream(file);
            Log.i("analyseFile",""+finFile.available());
            long fileSize = finFile.available();

            //letM
            int headerFir =finFile.read(fileByte,0,4);
            String headerFirStr = new String(fileByte,0,headerFir);
            Log.i("analyseFile",headerFirStr);
            //0048
            int headerSec =finFile.read(fileByte,4,4);
            String headerSecStr = new String(fileByte,4,headerSec);
            int headerSecSize=Integer.parseInt(headerSecStr);
            Log.i("analyseFile","headerSecStr="+headerSecStr+" headerSecSize="+headerSecSize);
            //header
            int offFir = 4 + 4;
            int headerThi =finFile.read(fileByte,offFir,headerSecSize);
            String headerThiStr = new String(fileByte,offFir,headerThi);
            Log.i("analyseFile",headerThiStr);

            String[] headerSplit0 = headerThiStr.split(";");
            String[] headerSplit1 = headerSplit0[0].split(" ");
            String[] headerSplit2 = headerSplit0[1].split(" ");
            String[] headerSplit3 = headerSplit0[2].split(" ");
            Log.i("analyseFile",headerSplit1[0]+"  "+headerSplit1[1]);
            Log.i("analyseFile",headerSplit2[0]+"  "+headerSplit2[1]);
            Log.i("analyseFile",headerSplit3[0]+"  "+headerSplit3[1]);

            if(fileSize<Integer.parseInt(headerSplit1[1])){
                try {
                    finFile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return;
            }else {
                //文件1          4+4+48
                int offSec = offFir+headerSecSize;
                foutFileFir = new FileOutputStream(filePath+"/" + headerSplit1[0]);
                int fileFir = finFile.read(fileByte,offSec,Integer.parseInt(headerSplit1[1]));
                foutFileFir.write(fileByte,offSec,fileFir);
                //文件2
                int offThi = offFir+headerSecSize+Integer.parseInt(headerSplit1[1]);
                foutFileSec = new FileOutputStream(filePath+"/" + headerSplit2[0]);
                int fileSecond =finFile.read(fileByte,offThi,Integer.parseInt(headerSplit2[1]));
                foutFileSec.write(fileByte,offThi,fileSecond);
                //文件3
                int offFou = offFir+headerSecSize+Integer.parseInt(headerSplit2[1]);
                foutFileThi = new FileOutputStream(filePath+"/" + headerSplit3[0]);
                int fileThird =finFile.read(fileByte,offFou,Integer.parseInt(headerSplit3[1]));
                foutFileThi.write(fileByte,offFou,fileThird);

                try {
                    finFile.close();
                    foutFileFir.close();
                    foutFileSec.close();
                    foutFileThi.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            headerFirStr = null;
            headerSecStr = null;
            headerThiStr = null;
            //解析 lt_conf.3 这个文件
            String result = getJson(filePath+"/lt_conf.3");
            Gson gson = new Gson();
            Fyconf ltconf = gson.fromJson(result, Fyconf.class);
            Log.d(TAG,"corebusiness-");
            Log.d(TAG,""+ltconf.devtype);
            Log.d(TAG,""+ltconf.mem);
            Log.d(TAG,""+ltconf.iface);
            Log.d(TAG,""+ltconf.alive);
            Log.d(TAG,""+ltconf.server);
            Log.d(TAG,""+ltconf.port);

            //01 执行业务代码
            doSocket(ltconf.server,ltconf.port);
//            Log.i(TAG,"------------");
//            editor.putString("devtype",ltconf.devtype);
//            editor.putString("mem",ltconf.mem);
//            editor.putString("iface",ltconf.iface);
//            editor.putString("alive",ltconf.alive);
//            editor.putString("server",ltconf.server);
//            editor.putString("port",ltconf.port);
//            editor.apply();
        }catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void doSocket(String server,String port){
        try {
            String result = NetworkUtil.doPost(API.getCoreUrl(server,port),NetConfig.getCoreParm());
            JSONObject json = new JSONObject(result);
            JSONObject vpnObj = json.getJSONObject("vpn");
            Vpnjson.Vpn vpn = new Vpnjson.Vpn();
            vpn.enable = vpnObj.getBoolean("enable");
            vpn.conntime = vpnObj.getLong("conn-time");
            vpn.timestamp = vpnObj.getLong("timestamp");
            vpn.tcpport = vpnObj.getInt("tcp-port");
            vpn.udpport = vpnObj.getInt("udp-port");
            vpn.ip = vpnObj.getString("ip");
            vpn.alive = vpnObj.getInt("alive");
            JSONObject proxyObj = json.getJSONObject("proxy");
            Vpnjson.Proxy proxy = new Vpnjson.Proxy();
            proxy.enable = proxyObj.getBoolean("enable");
            proxy.conntime = proxyObj.getLong("conn-time");
            proxy.timestamp = proxyObj.getLong("timestamp");
            proxy.host = proxyObj.getString("host");
            proxy.port = proxyObj.getString("port");
            proxy.alive = proxyObj.getInt("alive");

            vpn_ip = vpn.ip;//"83.136.128.2"
            vpn_tcpport = vpn.tcpport;
            vpn_udpport = vpn.udpport;

            proxy_ip = proxy.host;
            proxy_tcpport = Integer.parseInt(proxy.port);

            Log.e(TAG,"vpn："+vpn.ip);
            Log.e(TAG,"vpn："+vpn.tcpport);
            Log.e(TAG,"vpn："+vpn.udpport);

            mHandler.sendEmptyMessage(1);
            mHandler.sendEmptyMessageDelayed(2,100);
            mHandler.sendEmptyMessageDelayed(3,200);

        } catch (Exception e) {
            Log.i(TAG, ""+e.getMessage());
            //e.printStackTrace();
        }
    }

    //计算文件MD5值
    private String getFileMD5(String savePath) {
        File file = new File(savePath);
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        MessageDigest digest = null;
        FileInputStream in = null;
        byte buffer[] = new byte[1024];
        int len;
        try {
            digest = MessageDigest.getInstance("MD5");
            in = new FileInputStream(file);
            while ((len = in.read(buffer, 0, 1024)) != -1) {
                digest.update(buffer, 0, len);
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return bytesToHexString(digest.digest());
    }

    private String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    //删除文件
    private boolean delAllFile(String path) {
        boolean flag = false;
        File file = new File(path);
        if (!file.exists()) {
            return flag;
        }
        if (!file.isDirectory()) {
            return flag;
        }
        String[] tempList = file.list();
        File temp = null;
        for (int i = 0; i < tempList.length; i++) {
            if (path.endsWith(File.separator)) {
                temp = new File(path + tempList[i]);
            } else {
                temp = new File(path + File.separator + tempList[i]);
            }
            if (temp.isFile()) {
                temp.delete();
            }
            if (temp.isDirectory()) {
                delAllFile(path + "/" + tempList[i]);//先删除文件夹里面的文件
                delFolder(path + "/" + tempList[i]);//再删除空文件夹
                flag = true;
            }
        }
        return flag;
    }

    private void delFolder(String folderPath) {
        try {
            delAllFile(folderPath); //删除完里面所有内容
            String filePath = folderPath;
            filePath = filePath.toString();
            File myFilePath = new File(filePath);
            myFilePath.delete(); //删除空文件夹
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //已不使用**************************************************

    private void shutdown() {
        if (mScheduledExecutor != null) {
            mScheduledExecutor.shutdown();
            mScheduledExecutor = null;
        }
    }

    public void scheduledExecutor() {
        // 发送心跳包
        mScheduledExecutor = Executors.newSingleThreadScheduledExecutor();
        mScheduledExecutor.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {

            }
        }, 10, 10, TimeUnit.SECONDS);
    }

    private void ProxySocketTCP(final String ip, final int port){
//        byte [] a={1,1,1,1,};
//        byte [] b={3,3,3,1};
//        byte [] c={4,5,6,7};
//        Log.d(TAG,"ByteData length= "+ a.length);
//        Log.d(TAG,"ByteData length= "+ b.length);
//        Log.d(TAG,"ByteData length= "+ c.length);
//        ByteDataUtil.byteMergerAll(a,b,c);
//        byte [] d =ByteDataUtil.byteMerger(a,b);
//        ByteDataUtil.byteMerger(d,c);
//        Log.d(TAG,"byteMerger = "+   ByteDataUtil.byteMerger(d,c));
//        Log.d(TAG,"byteMergerAll = "+ ByteDataUtil.byteMergerAll(a,b,c));
//        Log.d(TAG,"ByteData length = "+ ByteDataUtil.byteMerger( ByteDataUtil.byteMerger(a,b),c).length);
//        Log.d(TAG,"ByteData length = "+ ByteDataUtil.byteMergerAll(a,b,c).length);
        /*
        uint32_t  magic;
        uint16_t  type;
        uint16_t  hdr_len;
        uint32_t  seq_id;
        uint32_t  debug_id;
        uint16_t  total_len;
         */

//        Log.d(TAG,"ByteData i2= "+ ByteDataUtil.toBinary("你好"));
//        Log.d(TAG,"ByteData Integer= "+ Integer.toBinaryString(42));
//        Log.d(TAG,"ByteData Long= "+ Long.toBinaryString(1000000000));

        new Thread() {
            @Override
            public void run() {
                try {
                    //数据
                    long magic = getUint32(0x12348898);
                    int type = getUint16(3);
                    long debug_id = getUint32(0);
                    int hdr_len = getUint16(30);
                    long seq_id = getUint32(0x12348898);
                    int total_len = getUint16(3);

                    Long.toBinaryString(magic);
                    Integer.toBinaryString(type);
                    Long.toBinaryString(debug_id);
                    Integer.toBinaryString(hdr_len);
                    Long.toBinaryString(seq_id);
                    Integer.toBinaryString(total_len);

                    Log.d(TAG,"socket Binary = "+Long.toBinaryString(magic));
                    Log.d(TAG,"socket Binary = "+Integer.toBinaryString(type));
                    Log.d(TAG,"socket Binary = "+Long.toBinaryString(debug_id));
                    Log.d(TAG,"socket Binary = "+Integer.toBinaryString(hdr_len));
                    Log.d(TAG,"socket Binary = "+Long.toBinaryString(seq_id));
                    Log.d(TAG,"socket Binary = "+Integer.toBinaryString(total_len));
                    byte[] outputData =  new byte[]{0x52, 0x03, 0x00, 0x30, 0x20, 0x40};
                    byte[] output = new byte[]{(byte)1,(byte)0,(byte)0,(byte)0,(byte)0,(byte)0};

//                    Byte.parseByte(Long.toBinaryString(magic));
//                    Byte.parseByte(Integer.toBinaryString(type));
//                    Byte.parseByte(Long.toBinaryString(debug_id));
//                    Byte.parseByte(Integer.toBinaryString(hdr_len));
//                    Byte.parseByte(Long.toBinaryString(seq_id));
//                    Byte.parseByte(Integer.toBinaryString(total_len));
//                    Log.d(TAG,"socket Byte = "+Byte.parseByte(Long.toBinaryString(magic)));
//                    Log.d(TAG,"socket Byte = "+Byte.parseByte(Integer.toBinaryString(type)));
//                    Log.d(TAG,"socket Byte = "+Byte.parseByte(Long.toBinaryString(debug_id)));
//                    Log.d(TAG,"socket Byte = "+Byte.parseByte(Integer.toBinaryString(hdr_len)));
//                    Log.d(TAG,"socket Byte = "+Byte.parseByte(Long.toBinaryString(seq_id)));
//                    Log.d(TAG,"socket Byte = "+Byte.parseByte(Integer.toBinaryString(total_len)));
//                    String dataStr = Long.toBinaryString(magic)+
//                    Integer.toBinaryString(type)+
//                    Long.toBinaryString(debug_id)+
//                    Integer.toBinaryString(hdr_len)+
//                    Long.toBinaryString(seq_id)+
//                    Integer.toBinaryString(total_len);

                    //建立socket
                    Socket socket = new Socket(ip,port);
                    Log.d(TAG,"socket state = "+socket.isConnected());
                    socket.setKeepAlive(true);
                    //socket.setSoTimeout(10000);//连接时长
                    //注：建立连接，不需要写数据给服务器
                    //直接去发送心跳


                    //发数据
                    OutputStream os = socket.getOutputStream();
                    //PrintWriter pw = new PrintWriter(os);
                    //pw.write(dataStr);
                    //pw.flush();
                    DataOutputStream pw = new DataOutputStream(os);
                    pw.write(output);
                    pw.flush();
//                    socket.shutdownOutput();

                    //读数据
                    InputStream is = socket.getInputStream();
                    DataInputStream in = new DataInputStream(is);
                    BufferedReader br = new BufferedReader(new InputStreamReader(in));
                    String count;
                    while((count = br.readLine()) != null){
                        Log.d(TAG,"socket server = "+count);
                    }
                    //关闭资源
                    br.close();
                    is.close();
                    pw.close();
                    os.close();
//                    socket.close();
                } catch (SocketException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    private void VpnSocketUDP(final String ip, final int port){

        new Thread() {
            @Override
            public void run() {
                try {
                    DatagramSocket ds = new DatagramSocket();
                    byte[] bytes = "Hello World".getBytes();
                    int length = bytes.length;
                    InetAddress address = InetAddress.getByName("127.0.0.1");
                    int port = 10110;
                    DatagramPacket dp = new DatagramPacket(bytes, length, address, port);
                    ds.send(dp);
                    ds.close();

                } catch (SocketException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }
    
    public short getUint8(short s){
        return (short) (s & 0x00ff);
    }

    public int getUint16(int i){
        return i & 0x0000ffff;
    }

    public long getUint32(long l){
        return l & 0x00000000ffffffff;
    }
    
    //将assets文件拷贝到指定目录下
    private void copyAssets(String filePath,String fileName) {
        File file = new File(filePath+fileName);
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            InputStream inputStream = getAssets().open(fileName);
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            // Transfer bytes from inputStream to fileOutputStream
            byte[] buffer = new byte[1024];
            int byteRead;
            while (-1 != (byteRead = inputStream.read(buffer))) {
                fileOutputStream.write(buffer, 0, byteRead);
            }
            inputStream.close();
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //【系统权限+ system/app】的静默安装
    private void systemShell(){
        String installfile = filePath+"/app-debug.apk";
        Log.i("analyseFile","-->"+installfile);
        int installState = ShellUtil.installSlient(installfile);
        if(installState == 0){
//            openApp("pag");
        }
    }

    //手机已root的静默安装
    private void rootShell(){
        //前提:需要root权限
        boolean hasRoot = ShellUtil.hasRootPermission();
        Log.d("analyseFile-hasRoot",""+hasRoot);
//        String commend = "pm install -i "+filePath+"/app-debug.apk";
//        ShellUtil.execCommand(commend,true,true);
//        int result = ShellUtil.execCommand(commend,true,true).result;
//        String responseMsg = ShellUtil.execCommand(commend,true,true).responseMsg;
//        String errorMsg = ShellUtil.execCommand(commend,true,true).errorMsg;
        ArrayList cmdList = new ArrayList();
        cmdList.add("cp " +
                "/storage/emulated/0/Android/data/com.feiyiproxy.letplatform/cache/app-debug.apk " +
                "/storage/emulated/0/Download");
        cmdList.add("pm uninstall com.feiyiproxy.corebusiness");
        cmdList.add("pm install "+filePath+"/app-debug.apk");
        ShellUtil.execCommand(cmdList,true,true);
        int result = ShellUtil.execCommand(cmdList,true,true).result;
        String responseMsg = ShellUtil.execCommand(cmdList,true,true).responseMsg;
        String errorMsg = ShellUtil.execCommand(cmdList,true,true).errorMsg;
        Log.d("analyseFile-result",""+result+"\n");
        Log.d("analyseFile-responseMsg",""+responseMsg+"\n");
        Log.d("analyseFile-errorMsg",""+errorMsg+"\n");
//        //02 开启app
//        openApp("pkg");
    }

    private void openApp(String pkg){
        //需要root
//        //启动服务  adb shell am startservice com.nandhan.myservice/.MyService
//        String commend = "am start com.feiyiproxy.corebusiness/.MainActivity";
//        ShellUtil.execCommand(commend,true,true);
//        int result = ShellUtil.execCommand(commend,true,true).result;
//        String responseMsg = ShellUtil.execCommand(commend,true,true).responseMsg;
//        String errorMsg = ShellUtil.execCommand(commend,true,true).errorMsg;
//        Log.d("cmdshell-result",""+result);
//        Log.d("cmdshell-responseMsg",""+responseMsg);
//        Log.d("cmdshell-errorMsg",""+errorMsg);
        //无需root
        PackageManager packageManager = getPackageManager();
        Intent intent = packageManager.getLaunchIntentForPackage(pkg);
        if(intent==null){
            Log.d("cmdshell-errorMsg","未安装");
        }else{
            startActivity(intent);
        }
    }

}
