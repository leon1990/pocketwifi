package com.feiyiproxy.pocketwifi.util;

import android.util.Log;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class NetworkUtil {

    public static final String TAG= "letPlatformService";

    /**
     *  doPost
     * @param urlPath
     * @return
     */
    public static String doPost(String urlPath,String param) {
        String result = "";
        BufferedReader reader = null;
        try {
            URL url = new URL(urlPath);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("Content-Length", String.valueOf(param.length()));
            connection.setRequestProperty("Content-Type","application/json; charset=UTF-8");
            connection.setRequestProperty("accept","application/json");
            OutputStream out = connection.getOutputStream();
            out.write(param.getBytes());
            out.flush();
            out.close();
            Log.e(TAG, "getResponseCode=" + connection.getResponseCode());
            if (connection.getResponseCode()==200){
                reader = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()));
                result = reader.readLine();
                Log.e(TAG, "result=" + result);

            }else {
                result = "404";
            }
        } catch (Exception e) {
            result = "404";
            e.printStackTrace();
        }finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * @param urlPath 下载路径
     * @param downloadDir 下载存放目录
     * @return 返回下载文件
     */
    public static File downloadFile(String urlPath, String downloadDir) {
        File file= null;
        try {
            URL url = new URL(urlPath);
            URLConnection urlConnection = url.openConnection();
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlConnection;
            httpURLConnection.setRequestProperty("Charset", "UTF-8");
            httpURLConnection.connect();

            int fileLength = httpURLConnection.getContentLength();
            Log.d(TAG,"file length---->" + fileLength);
            String filePathUrl = httpURLConnection.getURL().getFile();
            String fileFullName = filePathUrl.substring(filePathUrl.lastIndexOf(File.separatorChar) + 1);
            Log.d(TAG,"fileFullName---->" + fileFullName);
            BufferedInputStream bin = new BufferedInputStream(httpURLConnection.getInputStream());
            file = new File(downloadDir + File.separatorChar + fileFullName);
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            OutputStream out = new FileOutputStream(file);
            int size = 0;
            int len = 0;
            byte[] buf = new byte[1024];
            while ((size = bin.read(buf)) != -1) {
                len += size;
                out.write(buf, 0, size);
            }
            bin.close();
            out.close();
            Log.i(TAG,"bin文件下载success");
        } catch (MalformedURLException e) {
            file = null;
            e.printStackTrace();
        } catch (IOException e) {
            file = null;
            e.printStackTrace();
        }catch (Exception e) {
            file = null;
            e.printStackTrace();
        } finally {
            return file;
        }
    }

}
