package com.feiyiproxy.pocketwifi.netty.vpnudp;

import android.util.Log;

import com.feiyiproxy.pocketwifi.netty.NettyConstant;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.DatagramPacket;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.CharsetUtil;

/**
 * Created by lmh on 11/18/2017.
 */
public class VpnUdpClientHandler extends SimpleChannelInboundHandler<DatagramPacket> {

    public static final String TAG= "proxy_service_VpnUdp";


    private VpnUdpListener listener;

    public VpnUdpClientHandler(VpnUdpListener listener) {
        this.listener = listener;
    }

    /**
     * 连接成功
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        VpnUdpClient.getInstance().setConnectStatus(true);
        listener.onVpnUdpStatusConnectChanged(NettyConstant.VpnUdp_STATUS_CONNECT_SUCCESS);
    }

    /**
     * 和服务器断开了
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        VpnUdpClient.getInstance().setConnectStatus(false);
        listener.onVpnUdpStatusConnectChanged(NettyConstant.VpnUdp_STATUS_CONNECT_CLOSED);
        Log.e(TAG, "VpnUdp--------和服务器断开了");
    }

    /**
     * 客户端收到消息
     */
    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, DatagramPacket datagramPacket) throws Exception {

        String response = datagramPacket.content().toString(CharsetUtil.UTF_8);
        Log.e(TAG, "VpnUdp--------客户端收到消息 = "+response);
        listener.onVpnUdpResponse(response);
//        if(response.startsWith("结果：")){
//            System.out.println(response);
//            channelHandlerContext.close();
//        }
    }

	@Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {

		if (evt instanceof IdleStateEvent) {
			IdleStateEvent event = (IdleStateEvent) evt;
			if (event.state() == IdleState.READER_IDLE){
				//ctx.close();
			}else if (event.state() == IdleState.WRITER_IDLE){
                listener.onVpnUdpHeartbeat();
			}else if (event.state() == IdleState.ALL_IDLE){//都超时

            }
		}
		super.userEventTriggered(ctx, evt);
	}



    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        super.exceptionCaught(ctx, cause);

        ctx.close();
        cause.printStackTrace();
    }
}
